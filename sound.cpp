// This file is part of Project Cerulean

#include <SDL.h>
#include <iostream>

#include "sound.h"
static void audioCallback(void* userdata, Uint8 *stream, int len);

const unsigned SAMPLE_RATE = 48000;
const float SAMPLE_RATE_F = 96000.0f;

using namespace std;

bool initAudio(uint8_t* sound_ram){
	SynthState* state = new SynthState;
	state->data = reinterpret_cast<SynthData*>(sound_ram);
	state->voices[0].param = &state->data->voices[0];
	state->voices[1].param = &state->data->voices[1];
	state->voices[2].param = &state->data->voices[2];
	state->voices[3].param = &state->data->voices[3];
	state->voices[4].param = &state->data->voices[4];
	state->voices[5].param = &state->data->voices[5];
	state->voices[6].param = &state->data->voices[6];
	state->voices[7].param = &state->data->voices[7];

	state->voices[0].param->oscfreq = 110;
	state->voices[0].param->plswidth = 0x8000;
	state->voices[0].param->waves = 0b01000000;
	state->voices[0].param->hplevel = 0;
	state->voices[0].param->bplevel = 0;
	state->voices[0].param->lplevel = 0xFF;
	state->voices[0].param->pflevel = 0x00;
	state->voices[0].param->filtfreq = 0x1000;
	state->voices[0].param->filtres = 0xEFFF;

	SDL_AudioSpec* desired = 
		(SDL_AudioSpec*)
			malloc(sizeof(SDL_AudioSpec));

	desired
		->freq = SAMPLE_RATE;
	desired
		->format = AUDIO_S16;
	desired
		->channels = 2;
	desired
		->samples = 8192;
	desired
		->callback = audioCallback;
	desired
		->userdata = (void*)state;

	SDL_AudioSpec* obtained = 
		(SDL_AudioSpec*)
			malloc(sizeof(SDL_AudioSpec));
	if(!obtained){
		cerr
			<< "Failed to malloc memory for result AudioSpec!"
			<< endl;
		exit(1);
	}
	fprintf(stderr,
			"Desired audio parameters:\n"
			"\tfreq = %i\n"
			"\tformat = %04X\n"
			"\tchannels = %u\n"
			"\tsamples = %u\n",
			desired->freq,
			desired->format,
			desired->channels,
			desired->samples);
	if(SDL_OpenAudio(desired, obtained) < 0){
		cerr << "Couldn't open audio: " << SDL_GetError() << endl;
		exit(1);
	}
	fprintf(stderr,
			"Obtained audio parameters:\n"
			"\tfreq = %i\n"
			"\tformat = %04X\n"
			"\tchannels = %u\n"
			"\tsamples = %u\n",
			obtained->freq,
			obtained->format,
			obtained->channels,
			obtained->samples);
	SDL_PauseAudio(0);
}

static void audioCallback(void* userdata, Uint8 *stream, int len){
	SynthState* state = reinterpret_cast<SynthState*>(userdata);

	int16_t* samples = reinterpret_cast<int16_t*>(stream);
	unsigned channels = 2;
	unsigned sample_length = (unsigned)len/(sizeof(int16_t)*channels);

	float* working_buffer = new float[sample_length*channels];
	memset(working_buffer, 0, sizeof(working_buffer));

	state->voices[0].calculate(state, working_buffer, sample_length);

	static float last_l = 0;
	static float last_r = 0;

	for(unsigned idx = 0;
		idx < sample_length*channels;
		idx+=channels){
		float l = working_buffer[idx];
		float r = working_buffer[idx+1];
		samples[idx] = l * 32767.0;
		samples[idx+1] = r * 32767.0;

	}

	delete[] working_buffer;
}

int calculateWave(uint32_t phase, uint8_t* wave){
	// do a lerp
	unsigned offset_a = (phase / (1<<26)) & 0x3F;
	unsigned offset_b = ((phase / (1<<26)) + 1) & 0x3F;
	unsigned t = (phase % (1<<26)) >> 11; // put in 16bit range
	unsigned cpl_t = 0x10000 - t;
	
	int a,b,c=0;
	if(((int32_t)phase) > 0){
		a = *(wave+offset_a);
		b = *(wave+offset_b);
	}else{
		a = -*(wave+(0x3F - offset_b));
		b = -*(wave+(0x3F - offset_a));
	}
	c = (cpl_t*a + t*b) >> 8; // complementary mix = requires a shift of only 8 bits to &(x 0xFFFF).

	return c;
}

#include "math.h"


void SVFilter::calculate(const float in,
						 const float p_freq,
						 const float p_res){
	unsigned oversample = 4;
	float freqcoeff = 2.0 * sin(M_PI * p_freq / (SAMPLE_RATE_F * (float)oversample));
	float rescoeff  = min(2.0*(1.0 - pow(p_res, 0.25)), min(2.0, 2.0/freqcoeff - freqcoeff*0.5));
	while(--oversample){
		lpout = store2 + (freqcoeff * store1);
		hpout = in - lpout - (rescoeff * store1);
		bpout = (freqcoeff * hpout) + store1;
		npout = hpout + lpout;

		store1 = hpout;
		store2 = lpout;
	}
}

float LPFilter::calculate(const float p_in,
						 const float p_freq,
						 const float p_res){
	float in = p_in;
	float freqcoeff = 2.0 * sin(M_PI * p_freq / (SAMPLE_RATE_F * (float)oversample));
	float rescoeff  = p_res + p_res/(1.0 - freqcoeff);
	for(unsigned c = 0; c < cascade; c++){
		for(unsigned o = 0; o < oversample; o++){
			store = store + freqcoeff * (in - store + rescoeff * (out - store));
			out   = out + freqcoeff * (store - out);
		}
		in = out;
	}
	return out;
}

void SynthVoiceState::calculate(SynthState* state, float* buffer, unsigned count){
	float filtfreq = pow(param->filtfreq * 23.0 / 65536.0, 3); // 23*23*23 yields roughly 12KHz ish, whatever
	float filtres  = 0.8 ; // 1.0 / (0.5 + (param->filtres * 250.0 / 65536.0));
	unsigned plswidth = param->plswidth;
	for(unsigned idx = 0;
		idx < count;
		idx++){
		float osc = ((phase / (4294967296 / 65536)) > plswidth)? 0.8 : -0.8;
		bandlimit.calculate(osc,
							0.4*SAMPLE_RATE_F, // a bit under nyquist
							sin(M_PI / 4.0)); // 45-degree knee
		float prefilt = bandlimit.out;
		float output = ((param->pflevel/256.0) * prefilt);

		filter.calculate(prefilt, filtfreq, filtres);
		
		output += ((param->lplevel/256.0) * filter.lpout);
		output += ((param->hplevel/256.0) * filter.hpout);
		output += ((param->bplevel/256.0) * filter.bpout);
		output += ((param->nplevel/256.0) * filter.npout);
		
		if(idx == 0 || idx == 24)
			printf("%f %08X\n", output, phase);	

		float left = output*0.75;
		float right = output*0.75;
		buffer[2*idx] = left;
		buffer[2*idx+1] = right;
		phase += (pow(2,32) * 432.0 / SAMPLE_RATE_F);

		if((idx & 7) == 0) param->filtfreq--;
	}
}
