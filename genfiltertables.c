#include <stdio.h>
#include <stdint.h>
#include <math.h>

int main(){
	printf("f\n");
	unsigned index = 0;
	for (;index < 0x10000; index++){
		float floating = (2.0 * (sin(M_PI * (index * (20000.f/65536.f)) / (48000.0*4)))) * 0xFFFF;
		printf("%.0f,",
			floating);
		if((index&0xf) == 0xf)
			printf("\n");
	}


	printf("q\n");
	index = 0;
	for (;index < 0x10000; index++){
		printf("%.0f,",
			(1.0 / (2.0 + index)) * 0xFFFF);
		if((index&0xf) == 0xF)
			printf("\n");
	}
	return 0;
}
