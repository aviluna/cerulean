CXX := g++
CXXFLAGS := -Os -g
INCLUDES := -I/usr/include/SDL2/
LIBS := -lSDL2 -lz80
Z80ASM := z80asm

all: main

main.o: main.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

mmu.o: mmu.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

display.o: display.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

sound.o: sound.cpp
	$(CXX) -c -o $@ $(INCLUDES) $(CXXFLAGS) $^

test.bin: test.asm
	$(Z80ASM) -o $@ $^

main: main.o display.o mmu.o sound.o
	$(CXX) -o $@ $(LIBS) $(INCLUDES) $^ $(CXXFLAGS)

