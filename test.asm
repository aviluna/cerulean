org 0x0000

io_port:			equ 0x00
display_offx:		equ 0x40
display_offy:		equ 0x41
display_textl:		equ 0x42
display_texth:		equ 0x43
display_tilel:		equ 0x44
display_tileh:		equ 0x45
display_pall:		equ 0x46
display_palh:		equ 0x47
display_textfg:		equ 0x48
display_textbg: 	equ 0x49
display_curl:		equ 0x4a
display_curh:		equ 0x4b
display_drawtil:	equ 0x4c
display_drawtex:	equ 0x4d

palette_base:		equ 0x3560
palette_end:		equ palette_base + 3*256
_reset:
	jp _start
ds 100, 0 ; 100 bytes of zeros
msg:
db "Project Cerulean OK", 0
_start:
	ld de, msg
.loop:
	ld a, (de)
	or a, 0
	jr z, .endmsg
	out (io_port), a
	inc de
	jp .loop
.endmsg:
	ld b, 0
.palettefill:
	ld hl, palette_base
.filloop:
	inc (hl)
	inc hl
	inc (hl)
	inc hl
	inc (hl)
	inc hl
	ld a, h
	cp palette_end&0xFF
	jr nz, .filloop
	ld a, l
	cp palette_end>>8
	jr nz, .filloop
	jp .palettefill
padding:
ds 0xFFFFFF - $
