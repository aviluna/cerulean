display_offx:		equ 0x40
display_offy:		equ 0x41
org 0x0
_resetVector:
	jp main
ds 0x100-$
org 0x100
_interrupt_table:
_vblank_interrupt:
	dw vblank_handler
	ds 0x200-$
OK_string:
dm "Project Cerulean OK"
OK_string_end:
OK_string_len: equ OK_string_end - OK_string

Fib_string:
dm "th entry of the fibonnaci sequence is "
Fib_string_end:
Fib_string_len: equ Fib_string_end - Fib_string
cursorlowport: equ 0x40+11
cursorhighport: equ 0x40+10
main:
	ld   c, 0
	ld  hl, OK_string
	ld   b, OK_string_len
	otir
	ld   a, '\n'
	out (0),a

	ld  sp, 0x3540

	ld   a, 1 ; interrupt table at 0x0100
	ld   i, a
	im   2
	ei

	ld bc, 2
	ld de, 0
.mainloop:
	push bc
	pop  de
	push bc
	call consHex16

	ld   c, 0
	ld  hl, Fib_string
	ld   b, Fib_string_len
	otir

	pop  bc
	push bc
	call fibo

	push hl
	pop  de
	call consHex16
	ld a,'\n'
	out (0), a
	pop bc
	inc bc
	halt
	jp  .mainloop

vblank_handler:
	push af
	push bc
	push de
	push hl
	ld   a, (spinner_state)
	inc  a
	and  0xf
	ld   (spinner_state), a
	srl  a
	srl  a

	ld  hl, spinner_chars
	ld  bc, 0
	ld   c, a
	add hl, bc
	ld   a, (hl)
	ld  (0x2000+79), a

	ld  hl, 0
	add hl, sp
	ld  d, h
	ld  e, l

	in a, (cursorlowport)
	ld b, a
	in a, (cursorhighport)
	ld c, a
	push bc
	ld a,0xd8
	out (cursorlowport),a
	ld a,0x04
	out (cursorhighport),a
	call consHex16

	pop bc
	ld  a,b
	out (cursorlowport), a
	ld  a, c
	out (cursorhighport), a

	pop hl
	pop de
	pop bc
	pop af
	ei
	reti
spinner_state:
	db 0
spinner_chars:
	dm "-\\|/-\\|/"

fibo:
	push af
	ld   a, b
	cp   0
	jr   nz, .fibo_do
	ld   a, c
	cp   0
	jr   z, .fibo_ret0 ; if = 0
	cp   2
	jr   c, .fibo_ret1 ; if less than 3
.fibo_do:
	push de

	dec  bc
	push bc
	call fibo
	pop  bc ; pop last bc off stack
	push hl ; push fibo-1 result

	dec  bc
	call fibo
	; fibo-2 result now in hl
	pop  bc ; pop fibo-1 result

	add  hl, bc

	; result now in hl
	pop  de
	pop  af
	ret
.fibo_ret0:
	ld   hl, 0
	pop  af
	ret
.fibo_ret1:
	ld   hl, 1
	pop  af
	ret

Mul16x16:
; performs DEHL=BC*DE
	push af
	push bc
	ld   hl, 0
	ld    a, 16
.mul16x16loop:
	add  hl, hl
	rl    e
	rl    d
	jp   nc, .mul16x16nomul
	add  hl, bc
	jp   nc, .mul16x16nomul
	inc  de
.mul16x16nomul:
	dec   a
	jp   nz, .mul16x16loop
	pop  bc
	pop  af
	ret
Mul8x8:
; performs HL=DE*A
	push af
	push bc
	ld   hl, 0
	ld    b, 8
.Mul8x8loop:
	rrca
	jp   nc, .Mul8x8noadd
	add  hl, de
.Mul8x8noadd:
	sla   e
	rl    d
	djnz  .Mul8x8loop
	pop  bc
	pop  af
	ret
consHex16:
; prints the 16byte integer in DE to console with
; save registers
	push af
	push bc
	push de
	push hl
; get frame ptr in ix
	push ix
	ld   ix, 0
	add  ix, sp

; allocate 4 bytes for buffer
; initialized to "0000"
	ld   bc, 0x3030
	push bc
	push bc
; leave a pointer to the bottom of the buffer on the stack
	ld   hl, 0
	add  hl, sp
	push hl
; point hl to the top of the buffer
	push ix
	pop  hl
.consHex16loop:
	ld   a, e
	cp   0
	jr   nz, .consHex16getchar
	ld   a, d
	cp   0
	jr   z, .consHex16print
.consHex16getchar:
	ld   a, e
	and  0xf
	ld   b, 0
	ld   c, a
	push hl
	ld   hl, CHARS
	add  hl, bc
	ld   a, (hl)
	pop  hl
	dec  hl
	ld   (hl), a
.consHex16shift:
	push bc
	ld b, 4
.consHex16shiftloop:
	scf
	ccf
	ld   a, d
	rra
	ld   d, a
	ld   a, e
	rra
	ld   e, a
	djnz .consHex16shiftloop
	pop  bc
	jp   .consHex16loop
.consHex16print:
	ld   c, 0
	ld   b, 4
	pop  hl
	otir

; clear temporary allocation
	ld   sp, ix
	pop  ix

; restore actual registers
	pop  hl
	pop  de
	pop  bc
	pop  af
	ret
CHARS:
	dm "0123456789ABCDEF"
move_state:
	db 0
ds 0xFFFFFF-$
