#pragma once

// This file is part of Project Cerulean

#include <stdint.h>

#include <z80.h>

typedef struct __attribute__((__packed__)) mmu_struct{
	uint8_t mode;	//  0x0000 - 0x1FFF = always mapped to RAM 0x0000-0x1FFF (8k)
					//	0x2000 - 0x5FFF = display RAM  (16k)
					//	0x6000 - 0x6FFF = sound RAM    (4k)
					//	0x7000 - 0x8FFF = RAM window 1 (8k)
					//	0x9000 - 0xAFFF = RAM window 2 (8k)
					//  0xB000 - 0xCFFF = RAM window 3 (8k)
					//  0xD000 - 0xFFFF = RAM window 4 (12k)
	uint16_t	window_1; // RAM window base address most significant 16 bits
	uint16_t	window_2;
	uint16_t	window_3;
	uint16_t	window_4;
	// unused;
} MMUstate;

byte memMapperIn(int param, ushort address);
void memMapperOut(int param, ushort address, byte data);

byte ioMapperIn(int param, ushort address);
void ioMapperOut(int param, ushort address, byte data);

