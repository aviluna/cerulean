// This file is part of Project Cerulean

#include "mmu.h"
#include "main.h"

byte memMapperIn(int param, ushort address){
	switch(address>>12){
		case 0x0:
		case 0x1:
			return z80.main_ram[address];
		case 0x2:
		case 0x3:
		case 0x4:
		case 0x5:
			return z80.display_ram[address - 0x2000];
		case 0x6:
			return z80.sound_ram[address - 0x6000];
		case 0x7:
		case 0x8:
			return z80.main_ram
				[(z80.mmu.window_1 << 8) + (address - 0x7000)];
		case 0x9:
		case 0xa:
			return z80.main_ram
				[(z80.mmu.window_2 << 8) + (address - 0x9000)];
		case 0xb:
		case 0xc:
			return z80.main_ram
				[(z80.mmu.window_3 << 8) + (address - 0xb000)];
		case 0xd:
		case 0xe:
		case 0xf:
			return z80.main_ram
				[(z80.mmu.window_4 << 8) + (address - 0xd000)];
	}
/*
	if(address < 0x2000){
		// initial RAM page 0x0000-0x1FFF
		return z80.main_ram[address];
	}
	else if(address >= 0x2000 &&
			address <= 0x5fff){
		// display RAM 0x2000 - 0x5FFF
		return z80.display_ram[address - 0x2000];
	}
	else if(address >= 0x6000 &&
			address <= 0x6FFF){
		return z80.sound_ram[address - 0x6000];
	}
	else if(address >= 0x7000 &&
			address <= 0x8FFF){
		return *(z80.main_ram +
					(z80.mmu.window_1 << 8) +
						address - 0x7000);
	}
	else if(address >= 0x9000 &&
			address <= 0x9FFF){
		return *(z80.main_ram +
					(z80.mmu.window_2 << 8) +
						address - 0x9000);
	}
	else if(address >= 0xB000 &&
			address <= 0xCFFF){
		return *(z80.main_ram +
					(z80.mmu.window_3 << 8) +
						address - 0xB000);
	}
	else if(address >= 0xD000 &&
			address <= 0xFFFF){
		return *(z80.main_ram +
					(z80.mmu.window_4 << 8) +
						address - 0xD000);
	}*/
}

void memMapperOut(int param, ushort address, byte data){
	if(address < 0x2000){
		// initial RAM page 0x0000-0x1FFFF
		z80.main_ram[address] = data;
	}
	else if(address >= 0x2000 &&
			address <= 0x5fff){
		// display RAM 0x2000 - 0x5FFF
		z80.display_ram[address - 0x2000] = data;
	}
	else if(address >= 0x6000 &&
			address <= 0x6FFF){
		z80.sound_ram[address - 0x6000] = data;
	}
	else if(address >= 0x7000 &&
			address <= 0x8FFF){
		*(z80.main_ram +
			(z80.mmu.window_1 << 8) +
				address - 0x7000) = data;
	}
	else if(address >= 0x9000 &&
			address <= 0x9FFF){
		*(z80.main_ram +
			(z80.mmu.window_2 << 8) +
				address - 0x9000) = data;
	}
	else if(address >= 0xB000 &&
			address <= 0xCFFF){
		*(z80.main_ram +
			(z80.mmu.window_3 << 8) +
				address - 0xB000) = data;
	}
	else if(address >= 0xD000 &&
			address <= 0xFFFF){
		*(z80.main_ram +
			(z80.mmu.window_4 << 8) +
				address - 0xD000) = data;
	}
}

byte ioMapperIn(int param, ushort address){
#define IO_PORT 0x00
#define IO_DISPLAY_PORT 0x40
#define IO_DISPLAY_END  IO_DISPLAY_PORT + sizeof(DisplayMem)
#ifdef DEBUG_TRACE
	fprintf(stderr, "Port 0x%04X read stub\n", address);
#endif
	if((address & 0xFF) == IO_PORT){
		if(z80.input_fifo.empty()){
			return 0x00;
		}
		else{
			uint8_t result = z80.input_fifo.front();
			z80.input_fifo.pop();
			return result;
		}
	}
	else if((address & 0xFF) >= IO_DISPLAY_PORT &&
			(address & 0xFF) <= IO_DISPLAY_END){
		unsigned offset = (address & 0xFF) - IO_DISPLAY_PORT;
		return ((uint8_t*) &z80.display)[offset];
	}
}

void ioMapperOut(int param, ushort address, byte data){
#ifdef DEBUG_TRACE
	fprintf(stderr, "Port write 0x%02X @ 0x%04X\n", data, address);
#endif
	if((address & 0xFF) == 0){
		if(data == '\n'){
			z80.display.cursor += 80;
			z80.display.cursor -= z80.display.cursor % 80;
		}
		else{
			z80.z80.memWrite(0, z80.display.text_base + z80.display.cursor, data);
			z80.display.cursor++; // increment cursor
		}
		if(z80.display.cursor > 80 * 30) // wraparound if at end
			z80.display.cursor = 0;
	}
	else if((address & 0xFF) >= IO_DISPLAY_PORT &&
			(address & 0xFF) <= IO_DISPLAY_END){
		unsigned offset = (address & 0xFF) - IO_DISPLAY_PORT;
		((uint8_t*) &z80.display)[offset] = data;
		if(z80.display.offset_tiles){
			z80.display.tile_offx = z80.display.offset_x;
			z80.display.tile_offy = z80.display.offset_y;
		}
		else if(z80.display.offset_text){
			z80.display.text_offx = z80.display.offset_x;
			z80.display.text_offy = z80.display.offset_y;
		}
	}
}
