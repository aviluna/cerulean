#pragma once
// This file is part of Project Cerulean

#include <stdint.h>

/* 
 * There is plenty of potential for race conditions between the audio thread
 * and the emulated machine; this is intended behaviour.
 * Note, however, that timing will always be unreliable as Cerulean isn't
 * cycle-accurate.
*/

class SVFilter{
private:
	float store1, store2;
public:
	float lpout, hpout, bpout, npout;

	void calculate(const float in,
				   const float p_freq,
				   const float p_res);
};

class LPFilter{
private:
	float store;
	unsigned oversample;
	unsigned cascade;
public:
	float out; // aka store2

	float calculate(const float in,
				   const float p_freq,
				   const float p_res);

	// 1 = no oversampling
	// 1 = single cascade
	LPFilter(unsigned p_oversample = 1, unsigned p_cascade = 1):
		oversample(p_oversample),
		cascade(p_cascade),
		store(0.0f) {};
};

struct SynthState;

struct __attribute__((__packed__)) SynthVoice {
	uint16_t oscfreq;  // 1 = (4000/65536)Hz
	uint16_t plswidth; // pulse width for pulse generator, phaseadd for waves
	// 4 bytes
	uint8_t  velocity; // controls the envelope gate (with byte-sized 'velocity')
	// 5 bytes
	uint8_t  sync;     // synchronise fundamental frequency with osc1 (byte-sized flag!) (osc1 will not do anything by syncing with itself)
	// 6 bytes
	uint8_t  waves;    // each bit used to select from 0-5 programmed waves, sixth bit pulse, seventh noise muxed (bit-sized flags!)
	uint8_t  noise;    // used to select level of noise from 8bit lfsr mixed in
	uint16_t lfsrstate;
	uint8_t  noisemask;// mask for when to update the lfsr
	// 11 bytes
	uint8_t  attack;   // attack time
	uint8_t  decay;    // decay time
	uint8_t  sustain;  // sustain level
	uint8_t  release;  // release time
	// 15 bytes
	uint8_t  pflevel;  // pre-filter output level
	uint8_t  lplevel;  // level of post-lowpass-filter output
	uint8_t  bplevel;  // level of post-bandpass-filter output
	uint8_t  hplevel;  // level of post-highpass-filter output
	uint8_t  nplevel;  // level of post-notch-filter output
	uint16_t filtfreq; // 1 = (20000/65536)Hz
	uint16_t filtres;  // i dunno lol
	// 24
	uint8_t wavemul[6];// x-1 for each wave y except zero, f = (* (+ 1 x) oscfreq)
	// 30
	int32_t  stereopan; // 0 = centered, - = more left, + = more right
	// 32

	// Waves are 8bit,
	// 64 samples = first 180 degrees of oscillation at frequency,
	// Stored at +0x200 in sound ram
}; // 8 of these is at the base of soundram (* 32 8) = 256 (aka 0x100) bytes

struct SynthVoiceState{
	SynthVoice* param;
	uint32_t phase;
	SVFilter filter;
	LPFilter bandlimit;
	void calculate(SynthState* state, float* buffer, unsigned count);

	SynthVoiceState(): bandlimit(3,4) {}
};

struct __attribute__((__packed__)) SynthData{
	SynthVoice voices[8];
	uint8_t    waves[6*64];
};

struct SynthState{
	SynthData*      data;
	SynthVoiceState voices[8];
};

bool initAudio(uint8_t* sound_ram);
