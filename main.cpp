// This file is part of Project Cerulean

#include <iostream>
#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/mman.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL.h>

extern "C"{
	#include <z80.h>
}

#include <queue>

#include "display.h"
#include "mmu.h"
#include "main.h"
#include "sound.h"

State z80 = {0};

SDL_Surface * gfx_screen;
SDL_Window * gfx_window;

using namespace std;

uint8_t* mmapChunk(const char * filename, const unsigned size){
	int fd = open(filename, O_RDWR);
	if(!fd){
		perror("open");
		exit(1);
		return NULL;
	}
	uint8_t * map = (uint8_t *)
		mmap(	(caddr_t) 0,
				size,
				PROT_READ | PROT_WRITE,
				MAP_SHARED,
				fd,
				0);
	if(!map){
		perror("mmap");
		exit(1);
		return NULL;
	}
	close(fd);

	return map;
}

SDL_Renderer * renderer;

bool init(){
	srand(time(NULL));

	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		cerr	<< "SDL could not initialize! SDL_Error: " << SDL_GetError()
				<< endl;
		return false;
	}
	
	gfx_window = SDL_CreateWindow("Z80 Graphics",
								  SDL_WINDOWPOS_UNDEFINED,
								  SDL_WINDOWPOS_UNDEFINED,
								  640,
								  480,
								  SDL_WINDOW_SHOWN);
	if(!gfx_window){
		cerr	<< "Window could not be created: " << SDL_GetError()
				<< endl;
		return false;
	}

	renderer = SDL_CreateRenderer(gfx_window, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!renderer){
		cerr	<< "Renderer could not be created: " << SDL_GetError()
				<< endl;
		return false;
	}

 	gfx_screen = SDL_GetWindowSurface(gfx_window);
	
	z80.z80.memRead = memMapperIn;
	z80.z80.memWrite = memMapperOut;
	z80.z80.ioRead = ioMapperIn;
	z80.z80.ioWrite = ioMapperOut;
	
	z80.main_ram = mmapChunk("z80ram", 0xFFFFFF); // 16MByte
	z80.display_ram = mmapChunk("z80display",0x8000); // 16k
	z80.chargen = mmapChunk("z80char",0x0800); // 2k
	z80.tileset = mmapChunk("z80tileset",0xFFFF); // 64k
	z80.sound_ram = mmapChunk("z80sound",0x1000); // 4k

	initAudio(z80.sound_ram);

	z80.display.offset_x  = 0;
	z80.display.offset_y  = 0;
	z80.display.text_base = 0x2000;
	z80.display.tile_base = 0x3000;
	z80.display.palette_base = 0x3540;
	
	z80.display.textfg = 0xff;
	z80.display.textbg = 0x00;

	z80.display.draw_text = true;
	z80.display.draw_tiles = true;
	z80.display.offset_text = false;
	z80.display.offset_tiles = true;

	z80.mmu.window_1 = 0x2000; // right after first 8k block
	z80.mmu.window_2 = 0x4000; // right after that one
	z80.mmu.window_3 = 0x6000; // right after that one
	z80.mmu.window_4 = 0x8000; // right after that one

	Z80RESET(&z80.z80);

	return true;
}

void runCPU(unsigned num_T){
	Z80ExecuteTStates(&z80.z80, num_T);
}


int main(){
	if(!init()){
		return false;
	}
	
	while(true){
		SDL_Event event;
		while(SDL_PollEvent(&event)){
			if(event.type == SDL_QUIT){
				return 0;
			}
			else if(event.type == SDL_KEYDOWN){
				switch(event.key.keysym.sym){
				case SDLK_a:
					z80.input_fifo.push('a');
					break;
				}
			}
		}

//		runCPU(1667); // 100KHz @ 60fps
//		runCPU(16667); // 1MHz @ 60fps
//		runCPU(550000); // 33MHz @ 60fps

		renderGFX(renderer);

		const uint8_t VBLANK_INTERRUPT = 0x00;
		Z80INT(&z80.z80, VBLANK_INTERRUPT);
	}
	return 1;
}

