#pragma once

// This file is part of Project Cerulean

#include <z80.h>

#include <queue>

#include "mmu.h"
#include "display.h"

typedef struct state_struct{
	Z80Context z80;
	MMUstate mmu;
	DisplayMem display;
	uint8_t * main_ram;	// main RAM					 (16384k)
	uint8_t * display_ram; // display RAM				(16k)
	uint8_t * sound_ram; // sound RAM					( 4k)
	uint8_t * chargen; // chargen ROM (or RAM, whatever)( 2k)
	uint8_t * tileset; // tileset ROM/RAM				(64k)
	std::queue<uint8_t> input_fifo;
	unsigned counter;
} State;

extern State z80;

