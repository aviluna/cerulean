#pragma once

// This file is part of Project Cerulean

#include <SDL.h>

#include <stdint.h>

typedef struct __attribute__((__packed__)) display_struct{
	int8_t offset_x; // tile renderer normally has 16px of overdraw on all sides
	int8_t offset_y; // use this to move it around in the range -16..16
			// also can be used to move the tile and sprite renderers

	uint16_t text_base; // text renderer base address in terms of Z80 main bank
						// will be accessed with a stride of 80 for 30 lines
						// defaults to 0x2000-0x2960

	uint16_t tile_base; // tile renderer base address
						// will draw 42x32 tiles (16px overdraw on all edges)
						// if offset_* is 0 then the first visible tile will be
						// 1x1
						// tileset will be loaded from tileset ROM
						// defaults to 0x3000-0x3540
	
	uint16_t palette_base; // defaults to 0x3540-0x3840
	
	uint8_t textfg;	// uses 256col palette
	uint8_t textbg;	// ditto
	
	uint16_t cursor;// onscreen cursor position

	bool draw_tiles; // whether tiles should be rendered
	bool draw_text;	// whether text should be rendered

	bool offset_tiles;
			// whether writing to the offset registers should affect the
			// offset of the tile renderer
	bool offset_text;
			// whether writing to the offset registers should affect the
			// offset of the text renderer
	
	int8_t tile_offx;
	int8_t tile_offy;

	int8_t text_offx;
	int8_t text_offy;
} DisplayMem;

extern SDL_Surface * gfx_screen;
extern SDL_Window * gfx_window;

void renderGFX(SDL_Renderer * renderer);

