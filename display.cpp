// This file is part of Project Cerulean

#include <unistd.h>

#include <iostream>

#include <SDL.h>

#include "display.h"
#include "mmu.h"
#include "main.h"

using namespace std;

void renderTiles(SDL_Renderer * renderer){
	SDL_Surface * tileset_surface =
		SDL_CreateRGBSurfaceFrom(z80.tileset,
								 256, // w: 256
								 256, // h: 256
								 8,	  // depth: 8bpp
								 256, // pitch: 256bytes
								 0,0,0,0); //masks not used

	if(!tileset_surface){
		cerr
			<< "Failed to make tileset surface: " << SDL_GetError()
			<< endl;
		exit(1);
	}

	for(unsigned index; index < 256; index++){
		unsigned palette_addr = z80.display.palette_base + (index * 3);
		
		unsigned r = z80.z80.memRead(0, palette_addr);
		unsigned g = z80.z80.memRead(0, palette_addr+1);
		unsigned b = z80.z80.memRead(0, palette_addr+2);

		SDL_Color color = {
			.r = r,
			.g = g,
			.b = b
		};

		SDL_SetPaletteColors(tileset_surface->format->palette, &color, index, 1);
	}

	SDL_Texture * tileset_texture =
		SDL_CreateTextureFromSurface(renderer,
									 tileset_surface);
	if(!tileset_texture){
		cerr
			<< "Failed to make tileset texture: "
			<< SDL_GetError()
			<< endl;
		exit(1);
	}
	SDL_FreeSurface(tileset_surface);

	for(int y = -16; y < 496; y += 16){
		for(int x = -16; x < 656; x += 16){
			int screen_x = x + (z80.display.tile_offx % 16);
			int screen_y = y + (z80.display.tile_offy % 16);
			unsigned tile_addr =
				(z80.display.tile_base +
					42 * (y+16)/16 +
					(x+16)/16) & 0xFFFF;
			unsigned tile = z80.z80.memRead(0, tile_addr);
			SDL_Rect destrect = {
				.x = screen_x,
				.y = screen_y,
				.w = 16,
				.h = 16
			};
			SDL_Rect tilerect = {
				.x = (tile & 0xF) * 16,
				.y = (tile >> 4) * 16,
				.w = 16,
				.h = 16
			};
			SDL_RenderCopy(renderer,
						   tileset_texture,
						   &tilerect,
						   &destrect);
		}
	}

	SDL_DestroyTexture(tileset_texture);
}

void renderText(SDL_Renderer * renderer){
	uint16_t addr = z80.display.palette_base + (z80.display.textfg * 3);
	uint32_t fg = 0, bg = 0;
	fg = z80.z80.memRead(0, addr) |
		 (z80.z80.memRead(0, addr+1) << 8) |
		 (z80.z80.memRead(0, addr+2) << 16)|
		 (0xFF << 24);
	addr = z80.display.palette_base + (z80.display.textbg * 3);
	bg = z80.z80.memRead(0, addr) |
		 z80.z80.memRead(0, addr+1) << 8 |
		 z80.z80.memRead(0, addr+2) << 16;
	if(!z80.display.draw_tiles)
		bg |= (0xFF << 24);

	SDL_Surface * chargen_surface =
		SDL_CreateRGBSurface(0,
							 128,
							 128,
							 32,
							 0x000000FF,
							 0x0000FF00,
							 0x00FF0000,
							 0xFF000000);
	if(!chargen_surface){
		cerr
			<< "Failed to make chargen surface: " << SDL_GetError()
			<< endl;
		exit(1);
	}

	for(unsigned index = 0; index < 256; index++){
		for(unsigned cy = 0; cy < 8; cy++){
			uint8_t character = z80.chargen[(index*8) + cy];
			for(unsigned cx = 0; cx < 8; cx++){
				SDL_Rect rect = {
					.x = (index & 0xF) * 8 + cx,
					.y = (index >> 4)  * 8 + cy,
					.w = 1,
					.h = 1
				};
				SDL_FillRect(chargen_surface, &rect,
						(character & (1 << cx))?
							fg :
							bg);
			}
		}
	}
	
	SDL_Texture * chargen_texture =
		SDL_CreateTextureFromSurface(renderer, chargen_surface);
	if(!chargen_texture){
		cerr
			<< "Failed to create chargen texture: " << SDL_GetError()
			<< endl;
		exit(1);
	}

	SDL_FreeSurface(chargen_surface);

	for(unsigned y = 0; y < 480; y+=16){
		for(unsigned x = 0; x < 640; x+=8){
			int screen_x = x + (z80.display.text_offx % 16);
			int screen_y = y + (z80.display.text_offy % 16);
			unsigned text_addr =
				(z80.display.text_base +
					(y/16 * 80 + x/8)) &
						0xFFFF;
			unsigned texttile = z80.z80.memRead(0, text_addr);

			if((z80.counter & 0x1F) <= 0xF && z80.display.cursor == (y/16 * 80 + x/8)){
				SDL_Rect rect = {
					.x = screen_x,
					.y = screen_y,
					.w = 8,
					.h = 16
				};
				SDL_Color color;
				SDL_GetRenderDrawColor(renderer, &color.r, &color.g, &color.b, &color.a);
				SDL_SetRenderDrawColor(renderer, bg>>24, bg>>16, bg&0xff, 0);
				SDL_RenderFillRect(renderer, &rect);
				SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
			}
			else if(!texttile) continue; // don't do anything for the null tile 
			else{
				SDL_Rect srcrect = {
					.x = (texttile & 0xF)*8,
					.y = (texttile >>  4)*8,
					.w = 8,
					.h = 8
				};
				SDL_Rect dstrect = {
					.x = screen_x,
					.y = screen_y,
					.w = 8,
					.h = 16
				};
				SDL_RenderCopy(renderer, chargen_texture, &srcrect, &dstrect);
			}
		}
	}

	SDL_DestroyTexture(chargen_texture);
}

void renderGFX(SDL_Renderer * renderer){
	if(z80.display.draw_tiles)
		renderTiles(renderer);
	else
		SDL_RenderClear(renderer);
	
	if(z80.display.draw_text)renderText(renderer);
	
	z80.counter++;
	
	SDL_RenderPresent(renderer);
}
